

public class SequenceBot implements BotInterface {
    
    public static void main(String[] args) {
		String port = "15000";
		if (args.length == 2) {
			port = args[1];
		}
        Client client = new Client(new SequenceBot(), port);
        while(client.shouldStop == false) {
            try {
                Thread.sleep(10);
            } catch(InterruptedException e) {}
        }
    }
    
    public void turn(Client client, int turnNumber) {
        if (client.notMyPlanets.size() > 0) {
            MapObject toPlanet = client.notMyPlanets.get(0);
            for (MapObject planet : client.myPlanets) {
                if (planet.value > 1) {
                    client.send(planet, toPlanet, 1);
                    planet.value -= 1;
                }
            }
        }
        
        client.endTurn();
    }
}
