Чтобы создать нового Java-бота на основе существующего проекта, необходимо переименовать имя класса бота везде, где оно встречается. Логика бота должна быть описана в методе turn класса бота.
Запуск игры осуществляется следующим образом:
- Запустите терминал и перейдите в папку с игрой (cd ...)
- Запустите игру java -jar Assimilation.jar [options]
 	Опции:
 	-b1 <BotName> - имя бота игрока 1. Бот должен находиться в папке с игрой
 	-b2 <BotName> - имя бота игрока 2
 	Можно указать как одного, так и двоих ботов (можно не указывать ни одного). Боты, которые не указаны выбираются случайным образом из тех, которые находятся в папке с игрой.
 	-d <int> - задержка между ходами в миллисекундах. Если не указано, используется 150 мс
 	-c - закрыть игру после окончания боя
 	-dm - Debug Mode. В этом режиме игра загружает одного бота, а второго ждет на порту 15000. Вы можете запустить своего бота отдельно из любого места и подключиться к указанному порту. В этом режиме у ботов нет лимита на ход. (В обычном режиме у бота лимит 100 мс на ход)
 	
 	Примеры запуска 
 	java -jar Assimilation.jar -dm -b1 SequenceBot (Debug Mode, первый бот - SequenceBot, второго игра будет ждать на 15000м порту)
 	java -jar Assimilation.jar -b1 SequenceBot -b2 RandomBot -c (первый бот SequenceBot, второй бот RandomBot, закрыть игру после окончания боя)
 	java -jar Assimilation.jar (оба бота выбраны случайно)
 	java -jar Assimilation.jar -d 0 (оба бота выбраны случайно, задержка между ходами - 0 мс)
 	
------------ Правила игры -------------

- Игра представляет из себя пространство заполненное планетами.
- Каждая планета содержит некоторое количество кораблей.
- Каждая планета может быть нейтральна или принадлежать одному из игроков.
- Если планета принадлежит игроку, то она автоматически увеличивает количество своих кораблей на 1 за ход.
- Если планета нейтральна, то количество кораблей не увеличивается.
- Начальное количество кораблей для нейтральной планеты зависит от ее размера (чем больше, тем больше кораблей на ней находится).
- Если планета содержит N кораблей, то для её захвата игроком, игрок должен отправить на нее столько кораблей, чтобы по прибытию на эту планету их было N + 1.
  Например планета нейтральна и содержит 10 кораблей. До нее лететь 10 ходов. Значит игрок должен отправить 11 кораблей чтобы захватить её.
  Если планета принадлежит противнику, и содержит 10 кораблей, то игрок должен отправить 11 + 10 кораблей для ее захвата (с учетом увеличения количества кораблей на 1 за каждый ход)
- Игрок может отправлять любое доступное ему количество кораблей с планеты на любую другую планету, включая свои.
- За один ход можно отправить любое доступное количество кораблей с любого количества своих планет, включая многоразовую отправку с одной и той же планеты.
- Игра заканчивается когда все не нейтральные планеты принадлежат одному игроку, а так же не осталось кораблей другого игрока.

Правила захвата планеты следующие:
- При нападении на планету, из "её" кораблей вычитается сумма нападающих кораблей игрока.
- При одновременном нападении на планету двух игроков, сначала взаимно нейтрализуются корабли игроков, после чего оставшиеся корабли нападают на планету.
- Если количество кораблей на планете становится меньше нуля, то планета меняется свою принадлежность (переходит под контроль нападающего игрока), а так же оставшиеся корабли добавляются к планетным. Например если на планету с количеством кораблей 10 нападает 12 кораблей игрока, то планета переходит под контроль игрока с двумя кораблями на ней.
- Если в результате нападения на планету количество кораблей на ней стало 0, то планета не меняет принадлежность.
- Корабли не могут сражаться друг с другом, кроме случая одновременного нападения на планету.

Цель игрока - захватить все планеты противника, отправляя корабли со своих планет на планеты противника и нейтральные планеты.

------------ Логика бота ------------

Вся логика бота начинается в методе turn(). В этот метод передается ссылка на экземпляр класса клиента, а так же номер текущего хода. Клиент используется для связи с игрой, а так же извлечения состояния игры.
Основным и единственным классом, описывающим объекты в игре есть класс MapObject. Этим классом описываются планеты и корабли на игровом после.
Каждый ход должен заканчиваться вызовом client.endTurn();
На ход боту отводится 100мс, если за это время бот не сделал ход, то считается, что бот закончил ходить.
===
Описание полей MapObject:

double x
double y
Координаты объекта на игровом поле. Координаты изменяются от 0 до 1, где 0,0 - верхний левый угол, 1,1 - нижний правый угол.

int type
Тип объекта, где 1 - планета, 2 - корабль

int size
Размер планеты, где 1 - самый маленький размер, 3 - самый большой. Размер планеты влияет только на начальное количество кораблей на ней. Скорость роста кораблей на планете везде одинаковая: 1 корабль за ход.

int value
Значение объекта. Если это планета - то сколько кораблей на ней. Если корабль, то размер корабля, то есть сколько единиц в корабле.

int own
Принадлежность, где 0 - нейтральный объект (актуально только для планет), 1 - принадлежит Вам, 2 - принадлежит противнику.

int numberOfTurns
Количество ходов до цели. Поле относится только к кораблям и означает сколько ходов корабль будет двигаться до своей цели.

int id
ID объекта. Используется для отправки команды send, а так же для получения различных статистических данных по id объекта.

int fromId
ID объекта с которого корабль начал движение. Для планеты не актуально

int toId
ID объекта к которому корабль двигается. Для планеты не актуально
===

Единственная команда, которую можно отправлять игре - send, она описана у класса Client и имеет вид
client.send(from, to, count), где from и to экземпляры класса MapObject, являющиеся планетами. Count - количество кораблей для отправки. Если кораблей на планете не достаточно, команда будет проигнорирована игрой. За один ход можно отправить любое количество команд send.

Кроме этого доступны методы клиента:
distance(MapObject from, MapObject to) - расстояние между двумя объектами. Измеряется от 0 до 1, где 1 - ширина/высота игрового поля
turnsFromTo - количество ходов, которое корабль потратит пролетая расстояние от from до to

а так же списки объектов игры (MapObject):
allPlanets - список всех планет, где первая планета в списке - Ваша первая планета, последняя планета в списке - первая планета противника.
myPlanets - список Ваших планет
enemyPlanets - список планет противника
neutralPlanets - список нейтральных планет
notMyPlanets - список нейтральных планет и планет противника
allShips - список всех кораблей
myShips - список Ваших кораблей
enemyShips - список кораблей противника

