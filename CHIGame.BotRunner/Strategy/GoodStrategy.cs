﻿using CHIGame.Client.Bots.Strategy;
using CHIGame.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CHIGame.BotRunner.Strategy
{
    public class GoodStrategy : IBotStrategy
    {
        private class PossibleActions
        {
            public Decimal Score { get; set; }
            public Int32 FromId { get; set; }
            public Int32 ToId { get; set; }
            public Int32 Value { get; set; }
            public Int32 Distance { get; set; }
        }

        private class CurrentState
        {
            public List<Ship> MyShips { get; set; }
            public List<Planet> MyPlanets { get; set; }
            public List<Ship> OtherShips { get; set; }
            public List<Planet> AllOtherPlanets { get; set; }
        }

        public GoodStrategy() { }

        public IEnumerable<ActionItem> ComputeActions(DataParsingResult data)
        {
            List<ActionItem> result = new List<ActionItem>();

            CurrentState initialState = new CurrentState();

            initialState.MyShips = data.Ships.Where(s => s.Owner == 1).ToList();
            initialState.OtherShips = data.Ships.Where(s => s.Owner != 1).ToList();
            initialState.MyPlanets = data.Planets.Where(p => p.Owner == 1 && !IsUnderAttack(p, initialState.OtherShips)).ToList();
            initialState.AllOtherPlanets = data.Planets.Where(p => p.Owner != 1).ToList();

            Decimal agression = initialState.MyPlanets.Count() / (Decimal)data.Planets.Count();

            result.AddRange(CalculateActionsRecoursive(initialState, agression, data.Speed));

            result.Add(new ActionItem() { Type = ActionType.EndTurn });

            return result;
        }

        private IEnumerable<ActionItem> CalculateActionsRecoursive(CurrentState stateForNow, Decimal agression, Decimal speed)
        {
            List<ActionItem> result = new List<ActionItem>();
            List<PossibleActions> actions = new List<PossibleActions>();

            foreach (var otherPlanet in stateForNow.AllOtherPlanets)
            {               
                IEnumerable<Ship> shipsOnWay = stateForNow.MyShips.Where(s => s.ToId == otherPlanet.Id);

                Int32 alreadySentValue = shipsOnWay.Sum(i => i.Value);

                if (otherPlanet.Value < alreadySentValue) continue;

                foreach (var planet in stateForNow.MyPlanets)
                {
                    if (planet.Value == 0) continue;

                    Int32 distance = planet.GetTurnsCountTo(otherPlanet, speed);

                    Decimal score;
                    Int32 needToSend;

                    Decimal reverseAggression = (1 - agression * 2);
                    reverseAggression = reverseAggression == Decimal.Zero ? 0.00001M : reverseAggression;

                    if (otherPlanet.Owner == 2)
                    {
                        needToSend = otherPlanet.Value + (distance);
                        score = (distance + (needToSend - (planet.Value))) * reverseAggression;
                    }
                    else
                    {
                        needToSend = otherPlanet.Value - alreadySentValue + 1;
                        score = distance + (needToSend - planet.Value);
                    }

                    actions.Add(new PossibleActions() {
                          Score = score
                        , FromId = planet.Id
                        , ToId = otherPlanet.Id
                        , Value = Math.Min(needToSend, planet.Value)
                        , Distance = distance
                    });
                }
            }

            IEnumerable<ActionItem> newActions = ComputeBestActionItems(actions);

            if (!newActions.Any()) return result;

            stateForNow = ApplyStateForNewIteration(stateForNow, newActions, speed);

            result.AddRange(newActions);
            result.AddRange(CalculateActionsRecoursive(stateForNow, agression, speed));

            return result;
        }

        private CurrentState ApplyStateForNewIteration(CurrentState stateForNow, IEnumerable<ActionItem> actionItems, Decimal speed)
        {
            foreach (var item in actionItems)
            {
                Planet toPnalet = stateForNow.AllOtherPlanets.Find(i => i.Id == item.ToId);
                Planet fromPlanet = stateForNow.MyPlanets.Find(i => i.Id == item.FromId);

                if((fromPlanet.Value - item.Count) <= 0) fromPlanet.Value = 0;
                else fromPlanet.Value-= item.Count;

                Ship ship = new Ship();
                ship.X = fromPlanet.X;
                ship.Y = fromPlanet.Y;
                ship.Owner = fromPlanet.Owner;
                ship.NumberOfTurnsToTarget = fromPlanet.GetTurnsCountTo(toPnalet, speed);

                ship.Value = item.Count;
                ship.FromId = item.FromId;
                ship.ToId = item.ToId;

                stateForNow.MyShips.Add(ship);
            }

            return stateForNow;
        } 

        private IEnumerable<ActionItem> ComputeBestActionItems(IEnumerable<PossibleActions> items)
        {
            return items.GroupBy(p => p.FromId)
                .Select(i => i.OrderBy(m => m.Score).First())
                .Select(i => new ActionItem() {
                    Type = ActionType.Send,
                    Count = i.Value,
                    FromId = i.FromId,
                    ToId = i.ToId
                });
        }

        private Boolean IsUnderAttack(Planet myPlanet, IEnumerable<Ship> enemyShips)
        {
            List<Ship> attacketOfPlanet = enemyShips.Where(i => i.Owner != 1 && i.ToId == myPlanet.Id).ToList();

            Int32 attackValues = 0;
            Int32 allTurnsTuPlanet = 0;
                        
            foreach (var item in attacketOfPlanet)
            {
                allTurnsTuPlanet += item.NumberOfTurnsToTarget;
                attackValues += item.Value;
            }

            return myPlanet.Value <= (attackValues + (allTurnsTuPlanet / 3));
        }

    }
}
