﻿using System.Collections.Generic;
using CHIGame.Client.Bots.Strategy;
using CHIGame.Client.Models;

namespace CHIGame.BotRunner.Strategy
{
    public class DummyStrategy : IBotStrategy
    {
        public IEnumerable<ActionItem> ComputeActions(DataParsingResult data)
        {
            return new List<ActionItem>() {
                new ActionItem() { Type = ActionType.EndTurn }
            };
        }
    }
}
