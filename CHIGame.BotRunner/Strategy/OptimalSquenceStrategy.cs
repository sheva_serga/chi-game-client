﻿using CHIGame.Client.Bots.Strategy;
using CHIGame.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CHIGame.BotRunner.Strategy
{
    public class OptimalSquenceStrategy : IBotStrategy
    {
        private class PossibleActions
        {
            public Decimal Score { get; set; }
            public Int32 FromId { get; set; }
            public Int32 ToId { get; set; }
            public Int32 Value { get; set; }
            public Int32 Distance { get; set; }
        }

        public OptimalSquenceStrategy() { }

        public IEnumerable<ActionItem> ComputeActions(DataParsingResult data)
        {
            List<ActionItem> result = new List<ActionItem>();

            IEnumerable<Ship> myShips = data.Ships.Where(s => s.Owner == 1);
            IEnumerable<Planet> myPlanets = data.Planets.Where(p => p.Owner == 1);
            IEnumerable<Planet> allOtherPlanets = data.Planets.Where(p => p.Owner != 1);

            List<PossibleActions> actions = new List<PossibleActions>();

            Decimal agression = myPlanets.Count() / (Decimal)data.Planets.Count();

            foreach (var otherPlanet in allOtherPlanets)
            {               
                IEnumerable<Ship> shipsOnWay = myShips.Where(s => s.ToId == otherPlanet.Id);

                Int32 alreadySentValue = shipsOnWay.Sum(i => i.Value);

                if (otherPlanet.Value < alreadySentValue) continue;

                foreach (var planet in myPlanets)
                {
                    Int32 distance = planet.GetTurnsCountTo(otherPlanet, data.Speed);

                    Int32 needToSend = otherPlanet.Owner == 2
                        ? otherPlanet.Value - alreadySentValue + (distance / 2)
                        : otherPlanet.Value - alreadySentValue + 1;

                    Decimal score = otherPlanet.Owner == 2
                        ? (distance + (needToSend - planet.Value)) * (1 - agression)
                        : distance + (needToSend - planet.Value);

                    actions.Add(new PossibleActions() {
                          Score = score
                        , FromId = planet.Id
                        , ToId = otherPlanet.Id
                        , Value = Math.Min(needToSend, planet.Value)
                        , Distance = distance
                    });
                }
            }

            result.AddRange(
                actions.GroupBy(p => p.FromId)
                    .Select(i => i.OrderBy(m => m.Score).First())
                    .Select(i => new ActionItem() {
                        Type = ActionType.Send, 
                        Count = i.Value, 
                        FromId = i.FromId, 
                        ToId = i.ToId
                    })
            );

            result.Add(new ActionItem() { Type = ActionType.EndTurn });

            return result;
        }
    }
}
