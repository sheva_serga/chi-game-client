﻿using CHIGame.Client.Bots;
using CHIGame.Client.Bots.Strategy;
using CHIGame.Client.Bots.StreamReader;
using CHIGame.Client;
using CHIGame.Client.Common;
using System;
using CHIGame.BotRunner.Strategy;

namespace CHIGame.BotRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            GameConfigurations configurations = new GameConfigurations() {
                ServerHost = "localhost",
                ServerPort = args.Length == 1 ? Int32.Parse(args[0]) : 15000
            };

            IBotStrategy botStrategy = new GoodStrategy();
            ISocketExplorer socketExplorer = new JavaSocketExplorer();
            IInputProcessor inputProcessor = new InputMessageProcessor();

            GenericGameBot bot = new GenericGameBot(configurations, inputProcessor, botStrategy, socketExplorer);

#if DEBUG
            Console.WriteLine("Press 's' to stop bot");
            Console.WriteLine("Press 'r' to run bot");
            Console.WriteLine("Press 'q' to exit (bot will be stopped)");

            while (true)
            {
                var keyInfo = Console.ReadKey(true);

                if (keyInfo.KeyChar == 's')
                {

                    if (!bot.IsRunning) Console.WriteLine("Bot is already stopped");
                    else
                    {
                        bot.Stop();
                        Console.WriteLine("Bot has bee stoppet, to run bot again, pleas press 'r'");
                    }
                }

                if (keyInfo.KeyChar == 'q')
                {
                    bot.Stop();
                    break;
                }

                if (keyInfo.KeyChar == 'r')
                {
                    if (bot.IsRunning) Console.WriteLine("Bot is already running");
                    else
                    {
                        bot.Start();
                        Console.WriteLine("Bot has been started");
                    } 
                }
            }
#else
            bot.Start();
            Console.WriteLine("Bot has been started");
            bot.GetAwaiter().GetResult();
#endif
        }
    }
}
