﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CHIGame.Client.Models
{
    public class DataParsingResult
    {
        public Boolean IsShutDownRequest { get; set; }

        public List<Ship> Ships { get; set; }
        public List<Planet> Planets { get; set; }
        
        public Decimal Speed { get; set; }
        public Int32 TurnNumber { get; set; }
    }
}
