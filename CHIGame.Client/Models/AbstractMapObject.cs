﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CHIGame.Client.Models
{
    public abstract class AbstractMapObject
    {
        public Int32 Id { get; set; }
        public Int32 Value { get; set; }
        public Int32 Owner { get; set; }

        public Decimal X { get; set; }
        public Decimal Y { get; set; }

        public Decimal GetDistanceTo(AbstractMapObject to)
        {
            double x = (double)(X - to.X);
            double y = (double)(Y - to.Y);
     
            return (decimal)Math.Sqrt(Math.Abs(x * x - y * y));
        }

        public Int32 GetTurnsCountTo(AbstractMapObject to, Decimal speed)
        {
            Decimal distance = GetDistanceTo(to);
            Decimal turns = distance / speed;
            
            return (Int32)Math.Ceiling(turns) + 1;
        }
    }
}
