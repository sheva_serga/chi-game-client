﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CHIGame.Client.Models
{
    public class Ship : AbstractMapObject
    {
        public Int32 FromId { get; set; }
        public Int32 ToId { get; set; }
        
        public Int32 NumberOfTurnsToTarget { get; set; }
    }
}
