﻿using CHIGame.Client.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CHIGame.Client.Bots.StreamReader
{
    public class JavaSocketExplorer : ISocketExplorer
    {
        public async Task<String> ReadStringAsync(NetworkStream stream, CancellationToken token = default(CancellationToken))
        {
            int offset = 2;
            int readBytes = 0;
            byte[] buffer = new byte[2048];
            StringBuilder data = new StringBuilder();

            do
            {
                readBytes = await stream.ReadAsync(buffer, 0, buffer.Length, token);

                data.Append(Encoding.UTF8.GetString(buffer, offset, readBytes - offset));

                offset = 0;
            } while (stream.DataAvailable);

            return data.ToString();
        }

        public async Task WriteStringAsync(NetworkStream stream, String command, CancellationToken token = default(CancellationToken))
        {
            byte[] prefix = new byte[] { (byte)(0xff & (command.Length >> 8)), (byte)(0xff & command.Length) };

            byte[] data = Encoding.UTF8.GetBytes(command);

            if (stream.CanWrite)
                await stream.WriteAsync(prefix.Concat(data).ToArray(), token);
        }
    }
}
