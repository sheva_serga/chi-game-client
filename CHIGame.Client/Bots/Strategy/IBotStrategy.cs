﻿using CHIGame.Client.Models;
using System.Collections.Generic;

namespace CHIGame.Client.Bots.Strategy
{
    public interface IBotStrategy
    {
        IEnumerable<ActionItem> ComputeActions(DataParsingResult data);
    }
}
