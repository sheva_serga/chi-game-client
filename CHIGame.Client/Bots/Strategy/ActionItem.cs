﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CHIGame.Client.Bots.Strategy
{
    public enum ActionType
    {
        Send,
        EndTurn
    }

    public class ActionItem
    {
        public ActionType Type { get; set; }

        public Int32 ToId { get; set; }
        public Int32 FromId { get; set; }
        public Int32 Count { get; set; }
    }
}
