﻿using CHIGame.Client.Bots.Strategy;
using CHIGame.Client;
using CHIGame.Client.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CHIGame.Client.Bots
{
    public class GenericGameBot : GameSocketListener
    {
        private IBotStrategy _botStrategy;

        public GenericGameBot(GameConfigurations configurations, 
            IInputProcessor dataProcessor, 
            IBotStrategy botStrategy, 
            ISocketExplorer socketExplorer
        ) : base(configurations, dataProcessor, socketExplorer)
        {
            _botStrategy = botStrategy;
        }

        protected override async Task OnTurnStateResceived(StateResceivedEventArgs state)
        {
            var actions = _botStrategy.ComputeActions(state.ProcessingResult);

            var executer = CreateExecuter();

            foreach (ActionItem action in actions)
            {
                switch(action.Type)
                {
                    case ActionType.EndTurn:
                        await executer.EndTurnAsync();
                        break;
                    case ActionType.Send:
                        await executer.SendShipsAsync(action.FromId, action.ToId, action.Count);
                        break;
                }
            }
        }
    }
}
