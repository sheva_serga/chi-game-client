﻿using CHIGame.Client.Common;
using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CHIGame.Client.Command
{
    public class GameCommandExecuter
    {
        private static byte[] APPENDIX = new byte[] { 0xff, 0xff };

        private readonly TcpClient _tcpClient;
        private readonly ISocketExplorer _socketExplorer;

        public GameCommandExecuter(TcpClient tcpClient, ISocketExplorer socketExplorer)
        {
            _tcpClient = tcpClient;
            _socketExplorer = socketExplorer;
        }

        private async Task SendCommandAsync(String command)
        {
            if(_tcpClient.Connected)
                await _socketExplorer.WriteStringAsync(_tcpClient.GetStream(), command);
        }

        public async Task SendShipsAsync(Int32 fromId, Int32 toId, Int32 count) =>
            await SendCommandAsync($"#send:{fromId},{toId},{count}");
        

        public async Task EndTurnAsync() => 
            await SendCommandAsync("#endTurn");
        
    }
}
