﻿using System;
using System.Collections.Generic;
using System.Text;
using CHIGame.Client.Models;

namespace CHIGame.Client
{
    public interface IInputProcessor
    {
        DataParsingResult ParseDataString(string data);
    }
}
