﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CHIGame.Client.Models;

namespace CHIGame.Client
{
    public class InputMessageProcessor : IInputProcessor
    {
        public DataParsingResult ParseDataString(string data)
        {
            DataParsingResult result = null;

            if (data.Contains("stop", StringComparison.OrdinalIgnoreCase))
                result = new DataParsingResult() { IsShutDownRequest = true };
            else
            {
                result = new DataParsingResult();

                String[] dataSections = Regex.Split(data, "\\:?[a-z]+#").Skip(1).ToArray();

                result.Planets = ParsePlanets(dataSections[0]);
                result.Ships = ParseShips(dataSections[1]);

                var metadata = ParseMeta(dataSections[2]);

                result.Speed = metadata.Item1;
                result.TurnNumber = metadata.Item2;
            }

            return result;
        }

        private List<Planet> ParsePlanets(String data)
        {
            List<Planet> planets = new List<Planet>();

            if (String.IsNullOrEmpty(data)) return planets;

            Int32 id = 0;
            foreach (string item in data.Split(new [] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                string[] values = item.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (values.Length < 6) throw new FormatException("data has invalid format");

                Planet newPlanet = new Planet();
                newPlanet.Id = id++;
                newPlanet.X = Decimal.Parse(values[1]);
                newPlanet.Y = Decimal.Parse(values[2]);
                newPlanet.Value = Int32.Parse(values[3]);
                newPlanet.Owner = Int32.Parse(values[4]);
                newPlanet.Size = Int32.Parse(values[5]);

                planets.Add(newPlanet);
            }

            return planets;
        }

        private List<Ship> ParseShips(String data)
        {
            List<Ship> ships = new List<Ship>();

            if (String.IsNullOrEmpty(data)) return ships;

            Int32 id = 0;
            foreach (string item in data.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                string[] values = item.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (values.Length < 8) throw new FormatException("data has invalid format");

                Ship newShip = new Ship();
                newShip.Id = id++;
                newShip.X = Decimal.Parse(values[1]);
                newShip.Y = Decimal.Parse(values[2]);
                newShip.Value = Int32.Parse(values[3]);
                newShip.Owner = Int32.Parse(values[4]);
                newShip.FromId = Int32.Parse(values[5]);
                newShip.ToId = Int32.Parse(values[6]);
                newShip.NumberOfTurnsToTarget = Int32.Parse(values[7]);

                ships.Add(newShip);
            }

            return ships;
        }

        private Tuple<Decimal, Int32> ParseMeta(String data)
        {
            String[] metadata = data.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            Decimal speed = Decimal.Parse(metadata[0]);
            Int32 turnNum = Int32.Parse(metadata[1]);

            return Tuple.Create(speed, turnNum);
        }

    }
}
