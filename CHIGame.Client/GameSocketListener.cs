﻿using CHIGame.Client.Command;
using CHIGame.Client.Common;
using CHIGame.Client.Models;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CHIGame.Client
{
    public class StateResceivedEventArgs : EventArgs
    {
        public DataParsingResult ProcessingResult { get; private set; }

        public StateResceivedEventArgs(DataParsingResult result)
        {
            ProcessingResult = result;
        }
    }

    public abstract class GameSocketListener
    {
        private Object _syncRoot = new Object();

        private TcpClient _sListener;
        private CancellationTokenSource _tokerSource;

        private readonly ISocketExplorer _socketExplorer;
        private readonly IInputProcessor _dataProcessor;
        private readonly GameConfigurations _configurations;
        
        private Task<Task> _runningTask;

        public Boolean IsRunning { get; private set; }

        public GameSocketListener(GameConfigurations configurations, IInputProcessor dataProcessor, ISocketExplorer socketExplorer)
        {
            _socketExplorer = socketExplorer;
            _dataProcessor = dataProcessor;
            _configurations = configurations;
        }

        private async Task TryConnectAsync(CancellationToken token = default(CancellationToken))
        {
            _sListener = new TcpClient();

            Boolean isConnected = false;

            while (true)
            {
                if (token.IsCancellationRequested) return;

                try
                {
                    await _sListener.ConnectAsync(_configurations.ServerHost, _configurations.ServerPort);
                    isConnected = true;
                }
                catch { }

                if (isConnected) break;
                else await Task.Delay(5000, token);
            }
        }

        private async Task ReadFromSocketAsync(CancellationToken token = default(CancellationToken))
        {
            NetworkStream netStream = _sListener.GetStream();
            
            while (true)
            {
                if (token.IsCancellationRequested) return;

                if (!netStream.DataAvailable || !netStream.CanRead)
                {
                    await Task.Delay(100, token);

                    continue;
                }

                String data = await _socketExplorer.ReadStringAsync(netStream, token);

                DataParsingResult result = _dataProcessor.ParseDataString(data);

                if (result.IsShutDownRequest) Stop();
                else await OnTurnStateResceived(new StateResceivedEventArgs(result));
            }
        }

        protected GameCommandExecuter CreateExecuter() => new GameCommandExecuter(_sListener, _socketExplorer);

        public TaskAwaiter GetAwaiter()
        {
            lock (_syncRoot)
            {
                return IsRunning
                    ? _runningTask.Result.GetAwaiter()
                    : throw new InvalidOperationException("Bot is not running");
            }
        }

        public void Start()
        {
            lock (_syncRoot)
            {
                if(IsRunning == false)
                {
                    _tokerSource = new CancellationTokenSource();
                    _runningTask = TryConnectAsync(_tokerSource.Token)
                        .ContinueWith((r) => ReadFromSocketAsync(_tokerSource.Token), TaskContinuationOptions.OnlyOnRanToCompletion);

                    IsRunning = true;
                }
            }
        }

        public void Stop()
        {
            lock (_syncRoot)
            {
                if (IsRunning == true)
                {
                    _tokerSource.Cancel(false);

                    if (IsRunning) _sListener.Close();

                    _sListener?.Dispose();
                    _tokerSource?.Dispose();

                    IsRunning = false;  
                }
            }
        }

        protected abstract Task OnTurnStateResceived(StateResceivedEventArgs state);
    }
}
