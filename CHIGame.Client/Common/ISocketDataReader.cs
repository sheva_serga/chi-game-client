﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CHIGame.Client.Common
{
    public interface ISocketExplorer
    {
        Task<String> ReadStringAsync(NetworkStream stream, CancellationToken token = default(CancellationToken));
        Task WriteStringAsync(NetworkStream stream, String data, CancellationToken token = default(CancellationToken));
    }
}
