﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CHIGame.Client.Common
{
    public class GameConfigurations
    {
        public String ServerHost { get; set; }
        public Int32 ServerPort { get; set; }
    }
}
